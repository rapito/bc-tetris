package com.rapito.games.tetris.services;

import com.rapito.games.tetris.TetrisGame;

/**
 * Implements an specific IO behaviour to
 * read user input.
 * <p/>
 * Created by Robert Peralta on 9/5/2015.
 */
public interface InputListener
{

    /**
     * @return {@link TetrisGame.BoardMovement}
     */
    TetrisGame.BoardMovement readMove();
}
