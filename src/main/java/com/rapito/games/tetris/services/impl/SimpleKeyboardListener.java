package com.rapito.games.tetris.services.impl;

import com.rapito.games.tetris.TetrisGame;
import com.rapito.games.tetris.services.InputListener;

import java.io.IOException;

/**
 * Created by Robert Peralta on 9/5/2015.
 */
public class SimpleKeyboardListener implements InputListener
{

    /**
     * @return {@link TetrisGame.BoardMovement}
     */
    public TetrisGame.BoardMovement readMove()
    {
        TetrisGame.BoardMovement result = null;

        printMoveInstruction();

        try
        {
            int read = System.in.read();
            switch (read)
            {
                case 'a':
                    result = TetrisGame.BoardMovement.LEFT;
                    break;
                case 'd':
                    result = TetrisGame.BoardMovement.RIGHT;
                    break;
                case 'w':
                    result = TetrisGame.BoardMovement.ROTATE_LEFT;
                    break;
                case 's':
                    result = TetrisGame.BoardMovement.ROTATE_RIGHT;
                    break;
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        System.out.println();

        return result;
    }

    private void printMoveInstruction()
    {
        System.out.println("Please make a move: ");
        System.out.println("a (return): move piece left");
        System.out.println("d (return): move piece right");
        System.out.println("w (return): rotate piece counter clockwise");
        System.out.println("s (return): rotate piece clockwise");
        System.out.print("Your move: ");
    }
}
