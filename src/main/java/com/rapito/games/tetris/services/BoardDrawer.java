package com.rapito.games.tetris.services;

import com.rapito.games.tetris.entities.TBoard;

/**
 * Specifies a behaviour to draw the board to the screen.
 * Created by Robert Peralta on 9/5/2015.
 */
public interface BoardDrawer
{

    /**
     * Prints the board in any way for user visualization.
     */
    public void drawBoard(TBoard board);
}
