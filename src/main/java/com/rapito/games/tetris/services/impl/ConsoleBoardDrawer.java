package com.rapito.games.tetris.services.impl;

import com.rapito.games.tetris.entities.TBoard;
import com.rapito.games.tetris.services.BoardDrawer;

/**
 * Created by Robert Peralta on 9/5/2015.
 */
public class ConsoleBoardDrawer implements BoardDrawer
{

    /**
     * Prints the board in any way for user visualization.
     *
     * @param board
     */
    public void drawBoard(TBoard board)
    {
        int sizeX = board.getSizeX();
        int sizeY = board.getSizeY();
        int[][] field = board.getField();

        char draw = '*';

        for (int y = 0; y <= sizeY; y++)
        {
            System.out.print("*");
            for (int x = 0; x <= sizeX; x++)
            {
                if (y == sizeY | x == sizeX)
                {
                    draw = '*';
                }
                else
                {
                    int block = field[y][x];
                    draw = block >= 1 ? '*' : ' ';
                }
                System.out.print(draw);
            }
            System.out.println();
        }

    }
}
