package com.rapito.games.tetris.services;

import com.rapito.games.tetris.entities.TShape;

/**
 * Provides new shapes fo the {@link com.rapito.games.tetris.entities.TBoard}
 * <p/>
 * Created by Robert Peralta on 9/5/2015.
 */
public interface ShapeFactory
{

    /**
     * @return random {@link TShape} predefined by
     * this interfaces implementing class.
     */
    TShape createShape();
}
