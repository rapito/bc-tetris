package com.rapito.games.tetris.services.impl;

import com.rapito.games.tetris.entities.TShape;
import com.rapito.games.tetris.services.ShapeFactory;
import com.rapito.games.tetris.utils.Utils;

import java.util.Random;

/**
 * Created by Robert Peralta on 9/5/2015.
 */
public class SimpleShapeFactory implements ShapeFactory
{

    public enum Shapes
    {
        LINE, L, L_INVERTED, S, QUAD
    }

    static int[][][] prebuiltShapes = new int[][][]{
            {{0, 0}, {1, 0}, {2, 0}, {3, 0}}, //LINE
            {{0, 0}, {0, 1}, {0, 2}, {1, 2}}, //L
            {{1, 0}, {1, 1}, {1, 2}, {0, 2}}, //L_INVERTED
            {{1, 0}, {0, 1}, {1, 1}, {0, 2}}, //S
            {{0, 0}, {1, 0}, {0, 1}, {1, 1}}, //QUAD
    };

    /**
     * @return random {@link TShape} predefined by
     * this interfaces implementing class.
     */
    public TShape createShape()
    {
        TShape shape = new TShape();

        // Get a randome type from out enum
        int index = Utils.getRandom().nextInt(Shapes.values().length);
        Shapes type = Shapes.values()[index];

        // Build the shape from the predefiend block coordinates
        for (int i = 0; i < TShape.BLOCKS; i++)
        {
            int[] block = prebuiltShapes[type.ordinal()][i];
            shape.setX(i, block[0]);
            shape.setY(i, block[1]);
        }

        return shape;
    }
}
