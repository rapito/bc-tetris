package com.rapito.games.tetris;

/**
 * Main entry point for the game.
 * Created by Robert Peralta on 9/5/2015.
 */
public class Main
{

    private static final int SIZE_X = 20;
    private static final int SIZE_Y = 20;

    public static void main(String[] args)
    {
        TetrisGame game = new TetrisGame(SIZE_X, SIZE_Y);
        Thread t = new Thread(game);
        t.start();
    }
}
