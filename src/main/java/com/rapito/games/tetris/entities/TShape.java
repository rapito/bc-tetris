package com.rapito.games.tetris.entities;

import com.rapito.games.tetris.TetrisGame;
import com.rapito.games.tetris.utils.Utils;

/**
 * A TShape consists on 4 blocks, each with is
 * corresponding x,y coordinates on the {@link TBoard}
 * Created by Robert Peralta on 9/5/2015.
 */
public class TShape
{

    public static final int BLOCKS = 4;

    /**
     * int[] Specifies a given block
     * int[][] Specifies Block position on the {@link TBoard}
     */
    public int[][] position;
    public boolean landed;

    public TShape()
    {
        position = new int[BLOCKS][2];
    }

    /**
     * @param index Shape's Block index
     * @return X position of block
     */
    public int getX(int index)
    {
        return this.position[index][0];
    }

    /**
     * @param index Shape's Block index
     * @return Y position of block
     */
    public int getY(int index)
    {
        return this.position[index][1];
    }

    /**
     * @param index Shape's Block index
     * @return X position of block
     */
    public void setX(int index, int value)
    {
        this.position[index][0] = value;
    }

    /**
     * @param index Shape's Block index
     * @return Y position of block
     */
    public void setY(int index, int value)
    {
        this.position[index][1] = value;
    }

    /**
     * Calculates 2 new {@link TShape}:
     * 1 - By moving the shape according to the <code>movement</code>
     * 2 - By pushing downward the shape by 1 row.
     *
     * @return 2 (two) possible collision points of moving this pieces
     */
    public static TShape[] move(TShape shape, TetrisGame.BoardMovement movement)
    {
        TShape[] results = new TShape[2];
        TShape movementResult = null;

        switch (movement)
        {
            case LEFT:
            case RIGHT:
                movementResult = nudge(shape, movement);
                break;
            case ROTATE_LEFT:
            case ROTATE_RIGHT:
                movementResult = rotate(shape, movement);
                break;
        }

        results[0] = movementResult;
        results[1] = pushDown(movementResult);

        return results;
    }

    /**
     * Gets a new shape resulting of rotating the one sent in as a paramater
     */
    private static TShape rotate(TShape shape, TetrisGame.BoardMovement movement)
    {
        TShape result = new TShape();

        // Take the actual position off the shape so
        // we can rotate normally
        int posX = shape.getX(0);
        int posY = shape.getY(0);

        /**
         * Rotate/Flip each block
         */
        for (int i = 0; i < BLOCKS; ++i)
        {
            int targetX;
            int targetY;

            if (movement == TetrisGame.BoardMovement.ROTATE_RIGHT)
            {
                targetX = -(posY - shape.getY(i));
                targetY = (posX - shape.getX(i));
            }
            else
            {
                targetX = (posY - shape.getY(i));
                targetY = -(posX - shape.getX(i));
            }

            result.setBlockPosition(i, posX + targetX, posY + targetY);
        }

        return result;
    }

    /**
     * Gets a new shape resulting of pushing down by 1 row
     * the sent in as a paramater
     */
    private static TShape pushDown(TShape shape)
    {
        TShape result = new TShape();

        /**
         * Move each block's y position
         */
        for (int i = 0; i < BLOCKS; i++)
        {
            result.setX(i, shape.getX(i));
            result.setY(i, shape.getY(i) + 1);
        }

        return result;
    }

    /**
     * Gets a new shape resulting of nudging the sent in as a paramater
     */
    private static TShape nudge(TShape shape, TetrisGame.BoardMovement movement)
    {
        TShape result = new TShape();

        // move left or right accordingly
        int mx = movement == TetrisGame.BoardMovement.RIGHT ? 1 : -1;

        /**
         * Move each block's x position
         */
        for (int i = 0; i < BLOCKS; i++)
        {
            result.setBlockPosition(i, shape.getX(i) + mx, shape.getY(i));
        }

        return result;
    }

    /**
     * Gets any possible offset in x for where this
     * shape can be drawn in the <code>board</code>.
     */
    public int getRandomXOffset(TBoard board)
    {

        int offset = 0;
        int sizeX = board.getSizeX();
        int w = getWidth();

        offset = Utils.getRandom().nextInt(sizeX - w);

        return offset;

    }

    /**
     * Gets this shape max width
     */
    private int getWidth()
    {
        int minX = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE;

        for (int i = 0; i < BLOCKS; i++)
        {
            int x = getX(i);

            minX = x < minX ? x : minX;
            maxX = x > maxX ? x : maxX;
        }
        return maxX - minX;
    }

    /**
     * Gets this shape max position in X
     */
    public int getMaxX()
    {
        int maxX = Integer.MIN_VALUE;

        for (int i = 0; i < BLOCKS; i++)
        {
            int x = getX(i);
            maxX = x > maxX ? x : maxX;
        }
        return maxX;
    }

    /**
     * Gets this shape max position in X
     */
    public int getMaxY()
    {
        int maxY = Integer.MIN_VALUE;

        for (int i = 0; i < BLOCKS; i++)
        {
            int y = getY(i);
            maxY = y > maxY ? y : maxY;
        }
        return maxY;
    }

    public void setBlockPosition(int i, int x, int y)
    {
        setX(i, x);
        setY(i, y);
    }
}
