package com.rapito.games.tetris.entities;

import com.rapito.games.tetris.TetrisGame;

/**
 * The TBoard is responsible for handling all blocks
 * locations and a {@link TShape TShape's} movement and
 * rotation.
 * <p/>
 * Created by Robert Peralta on 9/5/2015.
 */
public class TBoard
{

    private final int sizeX;
    private final int sizeY;

    int[][] field;
    private TShape currentShape;

    public TBoard(int sizeX, int sizeY)
    {
        this.sizeX = sizeX;
        this.sizeY = sizeY;

        this.clear();
    }

    /**
     * Clears the board completely.
     */
    public void clear()
    {
        this.field = new int[sizeY][sizeX];
        this.currentShape = null;
    }

    /**
     * Pushes a new {@link TShape} to this
     * boards {@link #field}.
     *
     * @return <b>true</b> if the new shape fits on board.
     * <b>false</b> if it doesn't fit, thus the game should end.
     */
    public boolean setShape(TShape shape)
    {
        //TODO: random x
        //TODO: validation of any position at x
        int offsetX = shape.getRandomXOffset(this);
        boolean collides = shapeCollides(shape, offsetX, 0);

        if (collides) return false;

        this.currentShape = shape;
        placeShape(this.currentShape, offsetX, 0);

        return true;
    }


    /**
     * Moves the {@link #currentShape} accordingly.
     *
     * @return <b>true</b> if the {@link TShape} can make
     * that move and <b>false</b> if not.
     */
    public boolean moveShape(TetrisGame.BoardMovement movement)
    {
        //TODO: Validate outofBounds


        // Take the current shape out of the field first
        unplaceShape(this.currentShape);

        TShape targetShape = calculateMovedShape(movement);

        // If there arent any available moves, then set the
        // currentShape as landed
        if (targetShape == null)
        {
            if (!anyMovements())
            {
                this.currentShape.landed = true;
                return true;
            }
            else
            {
                targetShape = this.currentShape;
            }
        }

        // assign and place the shape into the board
        this.currentShape = targetShape;

        // Place the new shape
        placeShape(targetShape);

        return true;
    }

    private boolean anyMovements()
    {
        return calculateMovedShape(TetrisGame.BoardMovement.LEFT) != null ||
               calculateMovedShape(TetrisGame.BoardMovement.RIGHT) != null ||
               calculateMovedShape(TetrisGame.BoardMovement.ROTATE_LEFT) != null ||
               calculateMovedShape(TetrisGame.BoardMovement.ROTATE_RIGHT) != null;
    }

    private TShape calculateMovedShape(TetrisGame.BoardMovement movement)
    {
        TShape targetShape = this.currentShape;
        // Get both moves resulting of user input
        TShape[] results = TShape.move(this.currentShape, movement);

        // For each movement, check if any collided.
        // if they did, then set back into the field
        // out original shape and return
        for (int i = 0; i < results.length; i++)
        {
            TShape result = results[i];

            // our target shape will be the
            // last non colliding result
            targetShape = result;

            if (shapeCollides(result))
            {
                // If the moveDown collided then we
                // mark the shape as so
                if (i == 1)
                {
                    // Set the shape previous valid shape
                    targetShape = results[0];
                    targetShape.landed = true;
                }
                else
                {
                    // return null if first collides
                    return null;
                }
            }
        }
        return targetShape;
    }


    public boolean shapeLanded()
    {
        return this.currentShape.landed;
    }

    /**
     * Removes the <code>shape</code> from the {@link #field}
     */
    private void unplaceShape(TShape shape)
    {
        for (int i = 0; i < TShape.BLOCKS; i++)
        {
            int y = shape.getY(i);
            int x = shape.getX(i);
            this.field[y][x] -= 1;
        }
    }

    /**
     * @see #placeShape(TShape, int, int)
     */
    private void placeShape(TShape shape)
    {
        placeShape(shape, 0, 0);
    }

    /**
     * Places the <code>shape</code> into the {@link #field}
     */
    private void placeShape(TShape shape, int offsetX, int offsetY)
    {
        for (int i = 0; i < TShape.BLOCKS; i++)
        {
            int y = offsetY + shape.getY(i);
            int x = offsetX + shape.getX(i);
            this.field[y][x] += 1;

            shape.setBlockPosition(i, x, y);
        }
    }

    //<editor-fold desc="COLLISIONS">
    private boolean shapeCollides(TShape shape)
    {
        return shapeCollides(shape, 0, 0);
    }

    private boolean shapeCollides(TShape shape, int offsetX, int offsetY)
    {
        for (int i = 0; i < TShape.BLOCKS; i++)
        {
            int x = shape.getX(i);
            int y = shape.getY(i);

            if (blockCollides(offsetX + x, offsetY + y))
            {
                return true;
            }
        }

        return false;
    }

    private boolean blockCollides(int x, int y)
    {
        boolean result = true;
        try
        {
            result = this.field[y][x] > 0;

        }
        catch (IndexOutOfBoundsException e)
        {
            result = true;
        }
        return result;
    }

    //</editor-fold>


    public int getSizeX()
    {
        return sizeX;
    }

    public int getSizeY()
    {
        return sizeY;
    }

    public int[][] getField()
    {
        return field;
    }

    /**
     * Checks if the {@link #currentShape} is at the
     * limit of the {@link #field}
     */
    public boolean requireNewShape()
    {
        int posY = this.currentShape.getMaxY();
        return posY == getSizeY() - 1;
    }

    /**
     * Clears full lines
     */
    public void clearLines()
    {
        for (int y = this.sizeY - 1; y >= 0; y--)
        {
            boolean full = true;
            for (int x = 0; x < this.sizeY; x++)
            {
                // break if any empty
                if (getBlock(x, y) == 0)
                {
                    full = false;
                    break;
                }
            }

            // If the line is full then shift upper lines
            if (full)
            {
                shiftLinesTo(y);
            }
        }


    }

    /**
     * Shift down by 1 row all lines located over <code>targetY</code>
     */
    private void shiftLinesTo(int targetY)
    {
        for (int y = targetY; y >= 0; y--)
        {
            for (int x = 0; x < this.sizeY; x++)
            {
                int upperBlock = y == 0 ? 0 : getBlock(x, y - 1);
                setBlock(x, y, upperBlock);
            }
        }
    }

    /**
     * Sets the block's value to the sepcified location
     */
    private void setBlock(int x, int y, int value)
    {
        this.field[y][x] = value;
    }

    /**
     * Gets block value at given location
     */
    private int getBlock(int x, int y)
    {
        return field[y][x];
    }

}
