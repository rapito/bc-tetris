package com.rapito.games.tetris;

import com.rapito.games.tetris.entities.TBoard;
import com.rapito.games.tetris.entities.TShape;
import com.rapito.games.tetris.services.BoardDrawer;
import com.rapito.games.tetris.services.InputListener;
import com.rapito.games.tetris.services.ShapeFactory;
import com.rapito.games.tetris.services.impl.ConsoleBoardDrawer;
import com.rapito.games.tetris.services.impl.SimpleKeyboardListener;
import com.rapito.games.tetris.services.impl.SimpleShapeFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robert Peralta on 9/5/2015.
 */
public class TetrisGame implements Runnable
{

    private TBoard       board;
    private List<TShape> shapes;
    private TShape       currentShape;

    private ShapeFactory  shapeFactory;
    private InputListener inputListener;
    private BoardDrawer   boardDrawer;

    /**
     * Specifies posible moves
     * of any given {@link TShape} on the {@link TBoard}
     *
     * @see InputListener
     */
    public static enum BoardMovement
    {
        LEFT, RIGHT, ROTATE_LEFT, ROTATE_RIGHT
    }

    private boolean gameover;

    public TetrisGame(int sizeX, int sizeY)
    {
        this(sizeX, sizeY, new SimpleShapeFactory(), new ConsoleBoardDrawer(), new SimpleKeyboardListener());
    }

    public TetrisGame(int sizeX, int sizeY, ShapeFactory shapeFactory, BoardDrawer boardDrawer, InputListener inputListener)
    {
        // TODO: add nonNull validation
        this.shapeFactory = shapeFactory;
        this.boardDrawer = boardDrawer;
        this.inputListener = inputListener;

        this.board = new TBoard(sizeX, sizeY);
        this.shapes = new ArrayList<TShape>();
    }


    public void reset()
    {
        this.gameover = false;

        this.board.clear();
        this.shapes.clear();

        addNewShape();
    }

    private boolean addNewShape()
    {
        this.currentShape = shapeFactory.createShape();
        return this.board.setShape(currentShape);
    }

    public void run()
    {
        this.reset();

        BoardMovement movement;

        while (!this.gameover)
        {
            drawBoard();
            movement = readMove();
            if (movement != null)
            {
                boolean valid = this.board.moveShape(movement);
                boolean landed = this.board.shapeLanded();

                if (valid)
                {
                    if (landed || this.board.requireNewShape())
                    {
                        this.board.clearLines();
                        this.gameover = !addNewShape();
                    }
                }
                else
                {
                    invalidMove();
                }
            }
        }

        gameOver();
    }

    private void invalidMove()
    {
        System.out.println("Current shape cannot move there!");
    }

    private void gameOver()
    {
        System.out.println("Gameover!");
    }

    private void drawBoard()
    {
        this.boardDrawer.drawBoard(this.board);
    }

    private BoardMovement readMove()
    {
        return this.inputListener.readMove();
    }
}
