# Java CLI Tetris Game

Simple �text-mode� version of the Tetris game

I included a prebuilt binary in case you wanted to run it first 
before building. It's located on *dist/1.0/* folder, just execute ```run.sh/run.cmd```. 
(you need to remove the *.rename* extension, 
as google wont allow me to send those files over mail.)

## Required

This application is made with java and assumes you have installed 
**java** and **maven** in your path.

- Java jdk1.6
- Maven 3

## How to build

On your terminal/command prompt ```cd``` into project root:

- ```mvn package``` to build *bc-tetris-1.0.jar*.

## How to run

On your terminal/command prompt ```cd``` into project root:

- ```java -jar target/bc-tetris-1.0.jar``` to execute the game.


